extends Label

var blossom_count
var flower_color
var leaf_shape

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	flower_color = int(rand_range(0,3))
	blossom_count = int(rand_range(0,2))
	leaf_shape = int(rand_range(0,2))
	
	text = "Submit plants with " + convert_shape(leaf_shape) + " leaves, " + convert_color(flower_color) + " flowers, and "\
	+ convert_blooms(blossom_count) + " bloom(s). Good Luck!"

func convert_color(color):
	if color == 0:
		return "purple"
	elif color == 1:
		return "lavender"
	else:
		return "white"
		
func convert_blooms(blooms):
	if(blooms == 0):
		return "one"
	else:
		return "three"
		
func convert_shape(shape):
	if(shape == 0):
		return "round"
	else:
		return "pointy"
