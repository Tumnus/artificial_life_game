extends TextureButton

signal pick_up_ancestor_seed(flower_color, leaf_shape, blossom_count) 
var path = "res://Art Assets/Sprites/"

class Flower:
	var blossom_count: int
	var color: int
	var leaf_shape: int

var ancestor_seed_array = []

func add_ancestor_seed(color, shape, count):
	
	var flower = Flower.new()
	
	flower.blossom_count = count
	flower.color = color
	flower.leaf_shape = shape
	
	ancestor_seed_array.append(flower)

func _on_Ancestor_Seed_button_up():
	#are there any ancestor seeds in the inventory?
	if ancestor_seed_array.size() > 0:
		var seed_to_give : Flower = ancestor_seed_array.pop_back()

		emit_signal("pick_up_ancestor_seed", seed_to_give.color, seed_to_give.leaf_shape,  seed_to_give.blossom_count)
		
