extends Sprite

const CLEAR = -1

# Flower color
const PURPLE = 0
const LAVENDER = 1
const WHITE = 2

# Leaf shape
const ROUND = 0
const POINTY = 1

# Blossom count
const SINGLE = 0
const TRIPLE = 1

var _mouse_over = false
var _flower_color = CLEAR
var _leaf_shape = CLEAR
var _blossom_count = CLEAR
var flower_planted = false

var path = "res://Art Assets/Sprites/"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_Seed_drop_bulb(flower_color, leaf_shape, blossom_count):

		if _mouse_over and !flower_planted:
			_flower_color = flower_color
			_leaf_shape = leaf_shape
			_blossom_count = blossom_count
			_show_flower()
			flower_planted = true
			self.texture = load (path)
	
func reset_vase():
	self.texture = load("res://Art Assets/Sprites/Vase.png")
	_flower_color = CLEAR
	_leaf_shape = CLEAR
	_blossom_count = CLEAR
	flower_planted = false
	path = "res://Art Assets/Sprites/"

func _show_flower():
	set_path()
	
func set_path():
	path = "res://Art Assets/Sprites/"
	if  _leaf_shape == POINTY:
			path += "Pointy"
	else:
		_leaf_shape = ROUND
		path += "Round"

	if _blossom_count == TRIPLE: 
		path += " Triple" 
	else: 
		path += " Single" 
	if _flower_color == WHITE:
		path += " White"
	elif _flower_color == LAVENDER:
		path += " Lavender"
	else:
		path += " Purple"
		
	path += ".png"

func _on_Area2D_mouse_entered():
	_mouse_over = true
	


func _on_Area2D_mouse_exited():
	_mouse_over = false
	pass # Replace with function body.
	

func set_color(flower_color, blossom_count):
	_flower_color = flower_color
	
func set_shape(leaf_shape):
	_leaf_shape = leaf_shape
	
func set_count(blossom_count):
	_blossom_count = blossom_count



func _on_Next_Generation_button_up():
	reset_vase()
