extends Sprite

signal drop_bulb(flower_color, leaf_shape, blossom_count) # vases listen for a signal from the bulb

var path = "res://Art Assets/Sprites/"

const NUM_COLORS = 3 #used to calculate a random # in range 0 to x
const NUM_LEAF_SHAPES = 2
const NUM_BLOSSOM_COUNT = 2

var _is_following = false

export var flower_color: int
export var leaf_shape: int
export var blossom_count: int

func _ready():
	pass 
	
func _process(delta):
	if _is_following:
		follow_mouse()

func follow_mouse():
	position = get_global_mouse_position()
	self.visible = true
		
func _on_Get_Seed_button_button_up(): #the random seed
	_is_following = true
	randomize()
	_set_flower()
	$"Pick up sound".play()

	
func _input(event):
	if _is_following: 
		if event is InputEventMouseButton and \
		event.button_index == BUTTON_LEFT and \
		!event.pressed:
			$"Drop Sound".play()
			emit_signal("drop_bulb", flower_color, leaf_shape, blossom_count)	
			_is_following = false
			self.visible = true
			position.x = -5000
			position.y = -5000
	
func _set_flower():
	_set_flower_color()
	_set_blossom_count()
	_set_leaf_shape()
	
func get_flower():
	get_flower_color()
	get_blossom_count()
	get_leaf_shape()
		
func get_flower_color():	
	
	return flower_color
	
func _set_flower_color():
	randomize()
	flower_color = int(rand_range(0, NUM_COLORS))
	
func get_leaf_shape():
	return leaf_shape
	
func _set_leaf_shape():
	randomize()
	leaf_shape = int(rand_range(0, NUM_LEAF_SHAPES))
	
func get_blossom_count():
	return blossom_count
	
func _set_blossom_count():
	randomize()
	blossom_count = int(rand_range(0, NUM_BLOSSOM_COUNT))				

func _on_Ancestor_Seed_pick_up_ancestor_seed(color, shape, count):
	flower_color = color
	leaf_shape = shape
	blossom_count = count
	_is_following = true
	$"Pick up sound".play()	
