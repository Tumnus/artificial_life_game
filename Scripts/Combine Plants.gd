extends TextureButton

const NUM_PLANTS = 2
const PURPLE = 0
const LAVENDER = 1
const WHITE = 2

const POINTY = 0
const ROUND = 1

const ONE = 0
const THREE = 1

export var vase_1: NodePath
export var vase_2: NodePath

class Flower:
	var blossom_count: int
	var color: int
	var leaf_shape: int
	
var vase = Flower.new() #holds the offspring plant

func _on_Combine_Plants_Button_button_up():
	calculate_cross()
	print(vase.blossom_count)
	save_ancestor_seed()
	calculate_cross()
	print(vase.blossom_count)
	save_ancestor_seed()
	clear_plants()
	
func save_ancestor_seed():
	get_node("../../Buttons/Ancestor Seed").add_ancestor_seed(vase.color, vase.leaf_shape, vase.blossom_count) # goes out

func calculate_cross():
	#could add a check for null
	calculate_color_cross()
	calculate_shape_cross()
	calculate_count_cross()
	
func clear_plants():
	get_node(vase_1).reset_vase()
	get_node(vase_2).reset_vase()
	vase = Flower.new()	
	
func calculate_color_cross():
	
	var random_int = int(rand_range(0,100))
	var color1 = get_node(vase_1)._flower_color
	var color2 = get_node(vase_2)._flower_color
	
	if (color1 == PURPLE and color2 == PURPLE) || \
	(color1 == WHITE and color2 == WHITE):
		vase.color = color1
	
	elif(color1 == PURPLE and color2 == WHITE) || \
	(color1 == WHITE and color2 == PURPLE):
		vase.color = LAVENDER
		
	elif (color1 == WHITE and color2 == LAVENDER) || \
	(color1 == LAVENDER and color2 == WHITE):

		if random_int < 75:
			vase.color = LAVENDER
		else:
			vase.color = WHITE	
			
	elif (color1 == LAVENDER and color2 == LAVENDER):
		if random_int < 25:
			vase.color = PURPLE
		elif random_int <= 75:
			vase.color = LAVENDER
		else:
			vase.color = WHITE	
			
	elif(color1 == PURPLE and color2 == LAVENDER) || \
	(color1 == LAVENDER and color2 == PURPLE):	
		if random_int < 50:
			vase.color = PURPLE
		elif random_int >=50:
			vase.color = LAVENDER
	
func calculate_shape_cross():
	var shape1 = get_node(vase_1)._leaf_shape
	var shape2 = get_node(vase_2)._leaf_shape
	
	var shape = shape1
	if shape1 != shape2:
		shape = int(rand_range(0,2))
	vase.leaf_shape = shape
	
func calculate_count_cross():
	var count1 = get_node(vase_1)._blossom_count
	var count2 = get_node(vase_2)._blossom_count
	
	var count = count1
	
	if count1 != count2:
		count = int(rand_range(0,2))
	vase.blossom_count = count

	
	
	
	
	
	












#	
	




