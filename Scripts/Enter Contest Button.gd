extends Button

export var Game_Screen: NodePath
export var Score_Screen: NodePath
export var Score_Label: NodePath
export var Contest_Decision: NodePath

var total_score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _on_Enter_Contest_Button_button_up():
	
	for n in 9:
		#print(get_node("../../Vases/Vase" + String(n + 1)).name)
		total_score += calculate_plant_score(get_node("../../Vases/Vase" + String(n + 1)))
	
	display_score()
	
func display_score():
	#Do math here to figure out the score, and put it in the Score_Label variable
	get_node(Score_Label).text = "Congratulations! You scored " + String(total_score) + " points!"
	get_node(Game_Screen).visible = false
	get_node(Score_Screen).visible = true

func calculate_plant_score(plant):
	var score = 0
	if plant._flower_color == get_node(Contest_Decision).flower_color:
		score = score + 2
	if plant._leaf_shape == get_node(Contest_Decision).leaf_shape:
		score = score + 1
	if plant._blossom_count == get_node(Contest_Decision).blossom_count:
		score = score + 1
	return score
	
	
